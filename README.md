Git Assignment
======

This assignment is a quick literacy test of using git. You have all resources available to you (internet, the interviewer, etc). 

Please complete the following tasks:

 1. Create a new text file with "Hello World" in its contents, titled "world.txt"
 2. Create a second text file with "Hello Kai" in its contents, titled "kai.txt"
 3. Add each file to the repository in separate commits
 4. Squash the commits into one
 5. Revert the commit that adds the files